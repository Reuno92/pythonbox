class TuplesSchool:

    def tuple_assignment(self, firstWord, secondWord):
        a, b = firstWord, secondWord
        a,b = b, a

        # Must return secondWord, firstWord
        return a, b

    def form_list_from_tuple(self, first, second, third, fourth, fifth):
        a, *b, c = (first, second, third, fourth, fifth)

        # Must return second, third and fourth
        return b

    def tuples_return_index(self, my_tuples, index):
        return my_tuples.index(index)

    def count_tuples(self, my_tuples):
        return my_tuples.count()