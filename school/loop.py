from model.person import Person

"""
Loop Test

with array of string, number and object.

Attributes
----------
users : [Person] 
    List of Person with own attribute
    
Methods
-------
with_strings: None
    Return animal by string and their length
    
with_numbers(separator: str, reversed: bool = False) -> List[int]
    Return number by sort or reversed.

with_persons(is_active: bool) -> List[Dict[str, Any]]
    Return each person according isActive or not

by_gender(gender: str) -> List[Dict[str, Any]]
    Return each user by gender
"""

class Loop:
    users = [
        Person("Renaud", "Racinet", True, True),
        Person("Nadia", "Diallo", False, True),
        Person("John", "Doe", True, False)
    ]

    def with_strings():
        animals = ["cat", "dog", "bird"]

        for animal in animals:
            print(animal, len(animal))

    def with_numbers(separator: str, reversed=False):
        numbers = [4, 2, 1, 0, 3]

        numbers.sort(reverse=reversed)

        print(*numbers, sep=separator)

        result = []

        for number in numbers:
            result.append(number)

        return result

    def with_persons(is_active: bool):

        title = "Person which is active is"

        print(f"{title} {is_active}")
        print("_" * (len(title) + 1 + len(str(is_active))))

        result = []
        for user in Loop.users:
            if user.isActive == is_active:
                result.append(user.get_attributes())
        return result

    def by_gender(gender: str):

        if gender == "M" or gender == "Male" or gender == "male":
            result = []
            for user in Loop.users:
                if user.sex:
                    result.append(user.get_attributes())

            plural: bool = len(result) > 1

            if plural:
                print(f"Persons are male:")
                print("_________________")
            else:
                print(f"Person is male:")
                print("_______________")

            return result

        elif gender == "F" or gender == "Female" or gender == "female":
            result = []
            for user in Loop.users:
                if not user.sex:
                    result.append(user.get_attributes())

                    plural: bool = len(result) > 1

            if plural:
                print(f"\nPersons are female:")
                print("___________________")
            else:
                print(f"\nPerson is female:")
                print("_________________")

            return result

        else:
            ## Totally dumbed can't return Loop.users...
            result = []
            for user in Loop.users:
                result.append(user.get_attributes())
            return result
