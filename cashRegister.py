from model.chart import ChartModel
from model.product import Product

from decimal import *

"""
Cash register Class

Define logic of Cash register

Attributes
__________
total_chart : float
    Total of chart

currency : str
    Define currency, by default european union currency in 2024

chart : ChartModel
    define information of user with chart and his/her identity
    
Methods
_______
calculate_chart(self) -> None
    calculate cart with any items in chart informations according number and price

add_to_chart(self, product: Product) -> Any
    Add product in chart
    
get_attributes(self) -> Dict[str, Union[str, float, list, Dict[str, Any]]]
    return all information with total of chart, details of chart, currency, an user identity 
"""


class CashRegister:
    total_chart: float = 0
    currency: str

    def __init__(self, chart: ChartModel, currency="€"):
        getcontext().prec = 4
        self.chart = chart
        self.currency = currency

    def calculate_chart(self):
        chart_list = self.chart.chart_list
        result = 0

        for item in chart_list:
            result += Decimal(item.number) * Decimal(item.price)

        self.total_chart = result

    def add_to_chart(self, product: Product):
        self.chart.chart_list.append(product)
        self.calculate_chart()
        return self.chart.chart_list

    def get_attributes(self):
        chart = [item.__dict__ for item in self.chart.chart_list]

        result = {
            "total_chart": self.total_chart,
            "chart": chart,
            "currency": self.currency,
            "person": self.chart.person.get_attributes()
        }

        return result
