"""
Product Class Model
Define a product by its name, price at unit (unit or kilograms) and number (unit or kilograms)


Attributes
----------
name : str
    Name of product

price_by_unit : float
    price of product, by unit or kilograms

number : float
    number of product selected, by unit or kilograms

price_by_unit and number are floating number for have more flexibility
"""


class Product:

    def __init__(self, name: str, price: float, number: float):
        self.number = number
        self.price = price
        self.name = name

    @staticmethod
    def get_attributes():
        return Product.__dict__
