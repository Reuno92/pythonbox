from model.person import Person
from model.product import Product

"""

Chart Model

Attributes
----------
Person : Person
    Define an user
chart_list : [Product]
    Define Product which have selected by user
"""


class ChartModel:

    def __init__(
            self,
            person: Person,
            chart_list: [Product]
    ):
        self.person = person
        self.chart_list = chart_list

    @staticmethod
    def get_attributes():
        return ChartModel.__dict__