"""
Class Person

Attributes
----------
firstName : str
    First name of person
lastName : str
    Last name of Person
sex : bool
    Sex of Person,True is male and False is female
isActive : bool
    Define if Person is active
"""


class Person:

    def __init__(self, first_name: str, last_name: str, sex: bool, is_active: bool):
        self.lastName = last_name
        self.firstName = first_name
        self.sex = sex
        self.isActive = is_active

    def get_attributes(self):
        return self.__dict__
