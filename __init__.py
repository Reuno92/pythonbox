from model.chart import ChartModel
from model.person import Person
from model.product import Product
from school.loop import Loop
from cashRegister import CashRegister

class FirstClass:

    def __init__(self):
        # print(Person("Renaud", "Racinet", True, True).getAttributes())
        Loop.with_strings()
        print(Loop.with_numbers(" | ", True))
        print(Loop.with_persons(True))
        print(Loop.by_gender("F"), sep="\n", end="\n\n")

        session = CashRegister(
            ChartModel(
                Person("Renaud", "Racinet", True, True),
                []
            )
        )

        session.add_to_chart(
            Product("Cucumber", 0.65, 2)
        )

        session.add_to_chart(
            Product("Tomato", 3.25, 1.652)
        )

        session.add_to_chart(
            Product("Potato", 2.65, .987)
        )

        print(session.get_attributes())

FirstClass()