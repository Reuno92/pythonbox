import unittest
from cashRegister import CashRegister
from model.person import Person
from model.chart import ChartModel


class MyTestCase(unittest.TestCase):
    tested_class = CashRegister(
        ChartModel(
            Person("John", "Doe", True, True),
            []
        )
    )
    instance = tested_class.get_attributes()

    def test_person(self):
        first_name = self.instance['person']['firstName']
        last_name = self.instance['person']['lastName']
        sex = self.instance['person']['sex']
        is_active = self.instance['person']['isActive']

        self.assertEqual(first_name, "John")
        self.assertEqual(last_name, "Doe")
        self.assertTrue(sex)
        self.assertTrue(is_active, True)

    def test_currency(self):
        self.assertEqual(self.instance['currency'], '€')

    def test_origin_total_chart(self):
        self.assertEqual(self.instance['total_chart'], 0)

    def test_origin_chart(self):
        self.assertEqual(self.instance['chart'], [])


if __name__ == '__main__':
    unittest.main(verbosity=2, exit=False)
