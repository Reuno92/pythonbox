import unittest

from school.tuples import TuplesSchool


class MyTestCase(unittest.TestCase):
    testClass = TuplesSchool()

    def test_assignment(self):
        self.assertEqual(self.testClass.tuple_assignment("World", "Hello"), ("Hello", "World"))

    def test_swapping(self):
        self.assertEqual(self.testClass.form_list_from_tuple(1, 2, 3, 4, 5), [2, 3, 4])

    def test_tuples_index(self):
        self.assertEqual(self.testClass.tuples_return_index((1, 2, 3), 1), 0)
        self.assertEqual(self.testClass.tuples_return_index((1, 2, 3), 2), 1)


if __name__ == '__main__':
    unittest.main(verbosity=2, exit=False)
